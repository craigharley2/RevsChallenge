# Revs coding challenge

3 versions.

## No Dep - old school PHP
```shell script
php no_deps.php
```

## Functional Style - hip and fresh PHP
```shell script
php functional.php
```

## SQL
```mysql
SELECT      PLU, 
            SUM(quantity * price)
FROM        data
GROUP BY    PLU;
```
