<?php

require 'fn.php';

$rows = readCSV(__DIR__."/PLU Data.csv");

const DATE = 0;
const PLU = 1;
const QUANTITY = 2;
const PRICE = 3;

$result = [];
foreach ($rows as $row){
    if ($row){
        if (!isset($result[$row[PLU]])){
            $result[$row[PLU]] = 0;
        }

        $result[$row[PLU]] = $result[$row[PLU]] + ($row[QUANTITY] * $row[PRICE]);
    }
}

var_dump($result);

