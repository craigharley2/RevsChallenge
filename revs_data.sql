create table data
(
    id       int auto_increment
        primary key,
    date     date          null,
    PLU      int           null,
    quantity int           null,
    price    decimal(8, 2) null
);

INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (1, '2017-04-01', 9852, 36, 4.50);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (2, '2017-04-01', 9642, 20, 6.78);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (3, '2017-04-01', 9124, 69, 12.35);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (4, '2017-04-01', 9987, 17, 8.99);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (5, '2017-04-02', 9124, 37, 12.35);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (6, '2017-04-02', 9987, 71, 8.99);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (7, '2017-04-02', 9642, 91, 6.78);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (8, '2017-04-02', 9852, 28, 4.50);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (9, '2017-04-03', 9852, 93, 4.50);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (10, '2017-04-03', 9642, 18, 6.78);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (11, '2017-04-03', 9987, 21, 8.99);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (12, '2017-04-03', 9124, 82, 12.35);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (13, '2017-04-04', 9987, 73, 8.99);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (14, '2017-04-04', 9124, 11, 12.35);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (15, '2017-04-04', 9987, 48, 8.99);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (16, '2017-04-04', 9642, 31, 6.78);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (17, '2017-04-05', 9642, 11, 6.78);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (18, '2017-04-05', 9124, 13, 12.35);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (19, '2017-04-05', 9987, 98, 8.99);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (20, '2017-04-06', 9124, 31, 12.35);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (21, '2017-04-06', 9987, 27, 8.99);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (22, '2017-04-06', 9642, 68, 6.78);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (23, '2017-04-06', 9852, 98, 4.50);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (24, '2017-04-07', 9852, 53, 4.50);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (25, '2017-04-07', 9642, 70, 6.78);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (26, '2017-04-07', 9987, 50, 8.99);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (27, '2017-04-07', 9124, 55, 12.35);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (28, '2017-04-08', 9987, 44, 9.89);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (29, '2017-04-08', 9124, 3, 13.59);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (30, '2017-04-08', 9987, 44, 9.89);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (31, '2017-04-08', 9642, 58, 7.46);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (32, '2017-04-09', 9852, 55, 4.95);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (33, '2017-04-09', 9642, 47, 7.46);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (34, '2017-04-09', 9124, 41, 13.59);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (35, '2017-04-09', 9987, 6, 9.89);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (36, '2017-04-10', 9124, 8, 13.59);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (37, '2017-04-10', 9987, 42, 9.89);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (38, '2017-04-10', 9642, 87, 7.46);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (39, '2017-04-11', 9852, 16, 4.95);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (40, '2017-04-11', 9642, 96, 7.46);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (41, '2017-04-11', 9987, 57, 9.89);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (42, '2017-04-11', 9124, 8, 13.59);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (43, '2017-04-12', 9987, 36, 9.89);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (44, '2017-04-12', 9124, 89, 13.59);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (45, '2017-04-12', 9987, 45, 9.89);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (46, '2017-04-12', 9642, 36, 7.46);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (47, '2017-04-13', 9852, 7, 4.95);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (48, '2017-04-13', 9124, 26, 13.59);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (49, '2017-04-13', 9987, 69, 9.89);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (50, '2017-04-14', 9124, 49, 13.59);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (51, '2017-04-14', 9987, 40, 9.89);
INSERT INTO revs.data (id, date, PLU, quantity, price) VALUES (52, '2017-04-14', 9852, 77, 4.95);
