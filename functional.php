<?php

use Illuminate\Support\Collection;

require 'fn.php';
require './vendor/autoload.php';

$rows = readCSV(__DIR__."/PLU Data.csv");

const DATE = 0;
const PLU = 1;
const QUANTITY = 2;
const PRICE = 3;

var_dump(
    Collection
        ::make($rows)
        ->filter(fn($item): bool => is_array($item))
        ->groupBy(fn(array $arr): int => $arr[PLU])
        ->map(fn(Collection $collection): float =>
            $collection
                ->reduce(fn(float $carry, array $item): float
                    => $carry + ($item[PRICE] * $item[QUANTITY]),
                    0
                )
        )
);

