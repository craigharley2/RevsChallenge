<?php

/**
 * @param string $fileName
 *
 * @return array
 */
function readCSV(string $fileName): array
{
    $result = [];
    $handle = fopen($fileName, "r");
    if ($handle !== false) {
        $data = null;
        while ($data !== false) {
            $result[] = $data = fgetcsv($handle, 1000, ",");
        }
        fclose($handle);
    }

    return $result;
}
